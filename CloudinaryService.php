<?php
/*CloudinaryService.php
Symfony Service to connect to Cloudinary
Copyright (C) 2016,2017 Daniel Fredriksen
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/


namespace CYINT\ComponentsPHP\Services;
class CloudinaryService
{
    protected $Doctrine = null;

    function __construct($Doctrine)
    {  
        $this->Doctrine = $Doctrine;
        $this->setConfiguration();
    }

    function setConfiguration()
    {
        $Repository = $this->Doctrine->getRepository('CYINTSettingsBundle:Setting');
        $cloudinary = $Repository->findByNamespace('cloudinary');
        $cloudname = $cloudinary['cloudname'];
        $api_key = $cloudinary['api_key']; 
        $secret = $cloudinary['secret'];

        $this->setCloudName($cloudname);
        $this->setApiKey($api_key);
        $this->setSecret($secret);

        \Cloudinary::config(array( 
            "cloud_name" => $cloudname, 
            "api_key" => $api_key, 
            "api_secret" => $secret 
        ));

    }

    function setPublicKey($public_key)
    {
        $this->public_key = $public_key;
    }

    function setPrivateKey($private_key)
    {
        $this->private_key = $private_key;
    }

    function setSecret($secret)
    {
        $this->secret = $secret;
    }

    function getCloudName($cloudname)
    {
        return $this->cloudname = $cloudname;
    }

    function getApiKey($api_key)
    {
        return $this->api_key;
    }

    function getSecret($secret)
    {
        return $this->secret;
    }
}



?>
